docker pull postgres:10.0


PST_DIR=/path/to/where/postgres/files/will/be
PST_ROOT_DB=postgres
PST_ROOT_USR=postgres
PST_ROOT_PASS=xyz
PST_PORT=5432

PST_HOST=$(hostname -s)

PST_DB_NAME=trn0
PST_DB_PASS=notTheRealPass
PST_GROUP_NAME=public_data
PST_USER_NAME=public_user


mkdir -p $PST_DIR/data
chown -R root:root $PST_DIR

docker run -t -d --name postgres \
    -e POSTGRES_PASSWORD="$PST_ROOT_PASS" -e POSTGRES_USER=$PST_ROOT_USR -e POSTGRES_DB=$PST_ROOT_DB \
    -v $PST_DIR:/var/lib/postgresql \
    -v $PST_DIR/data:/var/lib/postgresql/data \
    --restart=always -p $PST_PORT:5432 \
    postgres:10.0

docker exec -it postgres bash -c "mkdir -p /var/lib/postgresql/$PST_GROUP_NAME/$PST_DB_NAME && chown -R postgres:postgres /var/lib/postgresql/$PST_GROUP_NAME"


psql -h $PST_HOST -U $PST_ROOT_USR -d $PST_ROOT_DB
---------------------------------
create role grp_$PST_GROUP_NAME;
create role $PST_USER_NAME login password "$PST_DB_PASS";
grant grp_$PST_GROUP_NAME to $PST_USER_NAME;

create tablespace tblsp_$PST_GROUP_NAME_$PST_DB_NAME location '/var/lib/postgresql/$PST_GROUP_NAME/$PST_DB_NAME';
create database db_$PST_DB_NAME tablespace=tblsp_$PST_GROUP_NAME_$PST_DB_NAME encoding 'UTF8' owner grp_$PST_GROUP_NAME;
grant ALL PRIVILEGES on DATABASE db_$PST_DB_NAME to grp_$PST_GROUP_NAME;
---------------------------------

psql -h $PST_HOST -U PST_ROOT_USR -d db_$PST_DB_NAME
---------------------------------
create schema $PST_DB_NAME authorization grp_$PST_GROUP_NAME;
---------------------------------

psql -h $PST_HOST -U $PST_USER_NAME -d db_$PST_DB_NAME
---------------------------------
\dn
---------------------------------

#### need to revoke write permissions to grp_$PST_GROUP_NAME
