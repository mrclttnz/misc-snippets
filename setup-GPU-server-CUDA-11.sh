
# Increased size of file systems
#  Filesystem                            Size  Used Avail Use% Mounted on
#  /dev/mapper/vg_root-lv_root           7.8G  112M  7.3G   2% /
#  /dev/mapper/vg_root-lv_usr            9.8G  4.4G  4.9G  48% /usr
#  /dev/sda1                             240M  140M   84M  63% /boot
#  /dev/mapper/vg_root-lv_home           5.9G  8.5M  5.6G   1% /home
#  /dev/mapper/vg_root-lv_tmp            7.9G   88M  7.4G   2% /tmp
#  /dev/mapper/vg_root-lv_var            3.9G  445M  3.3G  12% /var
#  /dev/mapper/vg_root-lv_var_log        7.9G   14M  7.5G   1% /var/log
#  /dev/mapper/vg_root-lv_var_log_audit  2.0G  3.0M  1.9G   1% /var/log/audit


# Configure server in the sysops project as edge server

# Run role: common 


# Install Docker 19.03
# Change in the sysops project the file roles/internal/docker/tasks/centos-install.yml so the size of docker-pool is 100G
# Change in the sysops project the file inventory/group_vars/all/docker.yml so the docker version is 19.03.0-3.el7
# Run role: docker




#  Info on Tensorflow version and corresponding CUDA version: https://www.tensorflow.org/install/gpu

# Install NVIDIA drivers
cd ~
wget http://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-repo-rhel7-10.1.168-1.x86_64.rpm 
yum install cuda-repo-rhel7-10.1.168-1.x86_64.rpm 
rm cuda-repo-rhel7-10.1.168-1.x86_64.rpm

yum-config-manager --disable cuda
yum install --enablerepo cuda,epel cuda


# Reboot the server for the driver to be working

# Post installation actions
# see https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#post-installation-actions

tee /etc/profile.d/cuda.sh <<-EOF
export PATH=/usr/local/cuda/bin:/usr/local/cuda/NsightCompute-2019.1\${PATH:+:\${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda/lib64\${LD_LIBRARY_PATH:+:\${LD_LIBRARY_PATH}}
EOF


# Install NVIDIA Persistence Daemon service
tee /etc/systemd/system/nvidia-persistenced.service <<-EOF
[Unit] 
Description=NVIDIA Persistence Daemon
Wants=syslog.target 

[Service] 
Type=forking
PIDFile=/var/run/nvidia-persistenced/nvidia-persistenced.pid
Restart=always
ExecStart=/usr/bin/nvidia-persistenced --verbose
ExecStopPost=/bin/rm -rf /var/run/nvidia persistenced 

[Install] 
WantedBy=multi-user.target
EOF

systemctl start nvidia-persistenced
systemctl enable nvidia-persistenced


# Install CUDA samples in order to run some tests
cd ~
mkdir cuda
cuda-install-samples-10.1.sh cuda

# Run deviceQuery sample
# check that last line of output of the tests says "Result = PASS"
cd cuda/NVIDIA_CUDA-10.1_Samples/bin/x86_64/linux/release/
./deviceQuery
./bandwidthTest





# Install NVIDIA container toolkit
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo | sudo tee /etc/yum.repos.d/nvidia-docker.repo
yum-config-manager --disable nvidia-docker
yum-config-manager --disable libnvidia-container
yum-config-manager --disable nvidia-container-runtime

yum install -y --enablerepo cuda,epel,nvidia-docker,libnvidia-container,nvidia-container-runtime nvidia-container-toolkit
systemctl restart docker



#### Test nvidia-smi with the latest official CUDA image
docker run --gpus all nvidia/cuda:9.0-base nvidia-smi



###  Use these commands to test our image once built
docker run -it --gpus all \
               --device=/dev/cpu \
               --device=/dev/nvidiactl --device=/dev/nvidia-uvm \
               --device=/dev/nvidia-uvm-tools --device=/dev/nvidia-modeset \
               --device=/dev/nvidia0 \
    <repo>/py3-gpu.marcol/initial-version bash



docker run -it --gpus all \
               --device=/dev/cpu \
               --device=/dev/nvidiactl --device=/dev/nvidia-uvm \
               --device=/dev/nvidia-uvm-tools --device=/dev/nvidia-modeset \
               --device=/dev/nvidia0 \
    <repo>/centos.edge bash




               --volume-driver=nvidia-docker --volume=nvidia_driver_361.48:/usr/local/nvidia:ro



docker run -d -p 8002:8002 --name py3-jhub.science.gpu \
    -v /etc/nsswitch.conf:/etc/nsswitch.conf:ro \
    -v /etc/pam.d:/etc/pam.d:ro \
    -v /opt/quest:/opt/quest:ro \
    -v /var/opt/quest:/var/opt/quest \
    -v /etc/opt/quest:/etc/opt/quest:ro \
    -v /usr/lib64/security:/usr/lib64/security:ro \
    -v /usr/lib64/libnss_vas4.so.2:/usr/lib64/libnss_vas4.so.2:ro \
    -v /usr/lib/libnss_vas4.so.2:/usr/lib/libnss_vas4.so.2:ro \
    -v /etc/localtime:/etc/localtime:ro \
    -v /home:/home \
    -v /nfs:/nfs:shared \
    --gpus all \
    --device=/dev/cpu \
    --device=/dev/nvidiactl --device=/dev/nvidia-uvm \
    --device=/dev/nvidia-uvm-tools --device=/dev/nvidia-modeset \
    --device=/dev/nvidia0 \
    --restart=always \
    <repo>/py3-jhub.gpu:latest \
    jupyterhub -f /etc/jupyterhub/jupyterhub_config.py --ip=127.0.0.1


docker run -d -p 80:80 -p 443:443 --name nginx.science.gpu \
    -v /etc/pki/tls:/etc/pki/tls:ro \
    -v /root/nginx/science.gpu.conf:/etc/nginx/nginx.conf:ro \
    --restart=always \
    <repo>/nginx:latest 


https://gb-slo-svr-0034/gpu/python3/user/marcol/tree/GPU


#===============================================
# https://www.tensorflow.org/install/source
#===============================================

cd ~
pip3 install mock
wget https://copr.fedorainfracloud.org/coprs/vbatts/bazel/repo/epel-7/vbatts-bazel-epel-7.repo && mv vbatts-bazel-epel-7.repo /etc/yum.repos.d/vbatts-bazel-epel-7.repo
yum install -y bazel



export CUDA_ROOT=/usr/local/cuda
export CUDA_INC_DIR=${CUDA_ROOT}/include
export PATH=${PATH}:${CUDA_ROOT}/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CUDA_ROOT}/lib64:${CUDA_ROOT}/extras/CUPTI/lib64
export C_INCLUDE_PATH=${C_INCLUDE_PATH}:${CUDA_ROOT}/include 

git clone https://github.com/tensorflow/tensorflow.git
cd tensorflow





##################################
wget --no-check-certificate https://xxxxxxx/resources/cudnn-10.0-linux-x64-v7.6.2.24.tgz
tar -xzvf cudnn-10.0-linux-x64-v7.6.2.24.tgz
cp cuda/include/cudnn.h /usr/local/cuda/include
cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*

##################
git clone https://github.com/Theano/libgpuarray.git
cd libgpuarray
mkdir Build
cd Build
# you can pass -DCMAKE_INSTALL_PREFIX=/path/to/somewhere to install to an alternate location
cmake .. -DCMAKE_BUILD_TYPE=Release # or Debug if you are investigating a crash
make
make install
cd ..
# This must be done after libgpuarray is installed as per instructions above.
python setup.py build
python setup.py install




pip3 install -U git+https://github.com/igraph/python-igraph.git --install-option="--c-core-url=https://github.com/igraph/igraph/archive/master.tar.gz"  && \
