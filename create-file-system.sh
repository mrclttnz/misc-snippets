## Create a new logical volume

lvcreate -L 5G -n lv_data vg_root

mkfs -t ext4 -v /dev/mapper/vg_root-lv_data

vi /etc/fstab
# Add the following line
/dev/mapper/vg_root-lv_data /data                  ext4    defaults        1 2

mkdir /data

mount -a


######################

## Extend a logical volume

lvextend -L 10G /dev/mapper/vg_root-lv_data

resize2fs /dev/mapper/vg_root-lv_data
