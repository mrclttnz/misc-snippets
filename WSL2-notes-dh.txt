# Instructions here: https://learn.microsoft.com/en-us/windows/wsl/install-manual
#                    https://learn.microsoft.com/en-us/windows/wsl/setup/environment

# From Powershell

# Enable the Windows Subsystem for Linux
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

# Enable Virtual Machine feature
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

# Reboot laptop


# Download the Linux kernel update package and run it as administrator
# Download from here: https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi
# might end execution without doing anything if the version you have is more advanced

wsl --set-default-version 2


# If the version of wsl does not come from Windows Store, the following command should update it to the Windows Store version
wsl --update

# Install Ubuntu 20.04 LTS
wsl --install -d Ubuntu-20.04


# Changes to VPN config to allow 
# enable the "Allow local (LAN) access when using VPN (if configured)" option within the configuration of Cisco AnyConnect
# Before connecting, select the EMEA_UK_VPN_Watford_Tunnel-All profile, as the other profiles won't send WSL2 traffic through the VPN (you won't be able to reach internal services like gitlab).  <-- This doesn't seem to be required. EMEA_UK_VPN_Watford is working fine for me



# Changes to config files in the image

# Add the following to the file /etc/wsl.conf (create it if it does not exist already).
# All options available at https://docs.microsoft.com/en-us/windows/wsl/wsl-config#configuration-options

	# Set whether WSL supports interop process like launching Windows apps and adding path variables. Setting these to false will block the launch of Windows processes and block adding $PATH environment variables.
	[interop]
	#enabled = false
	appendWindowsPath = false

	# This avoids /etc/resolv.conf is automatically rewritten at every restart
	[network]
	generateResolvConf = false
	localhostForwarding = true


# Adjust /etc/resolv.conf to use UK Watford DNS

rm /etc/resolv.conf
cat <<EOF > /etc/resolv.conf
search dunnhumby.co.uk
# UK watford dns order:
nameserver 10.96.16.111
nameserver 10.96.16.112
nameserver 10.128.24.12
nameserver 8.8.8.8
EOF



###  Proxy settings
sudo bash
cat <<EOF > /usr/local/sbin/set-proxy
# BEGIN set proxy vars
export http_proxy="http://securecomms.int.dh:3128"
export HTTP_PROXY=$http_proxy
export https_proxy=$http_proxy
export HTTPS_PROXY=$http_proxy
export no_proxy=".dunnhumby.co.uk,.dunnhumby.com,*.dunnhumby.co.uk,*.dunnhumby.com,.kssretail.local,localhost,127.0.0.1,tunnel.cloudproxy.app,10.0.2.15,tunnel.cloudproxy.iap,.googleapis.com,*.googleapis.com,googleapis.com,.cloud.google.com,*.cloud.google.com,gcr.io,eu.gcr.io,us.gcr.io,asio.gcr.io,.gcr.io,192.168.49.2"
export NO_PROXY=$no_proxy
export REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt
# END set proxy vars
EOF

cat <<EOF > /usr/local/sbin/unset-proxy
# BEGIN unset proxy vars
unset no_proxy
unset NO_PROXY
unset http_proxy
unset HTTP_PROXY
unset https_proxy
unset HTTPS_PROXY
# END unset proxy vars
EOF

chmod +x /usr/local/sbin/set-proxy /usr/local/sbin/unset-proxy

# Add the set-proxy command at the end of /etc/profile.d/proxy.sh:
cat <<EOF > /etc/profile.d/proxy.sh
# Set the proxy
. set-proxy
EOF

# Add the set-proxy command at the end of /root/.bashrc
cat <<EOF > /root/.bashrc
# Set the proxy
. set-proxy
EOF

# Set timezone to CET
rm -f /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Rome /etc/localtime

# Run a system update
sudo bash
. set-proxy
sudo apt update && sudo apt upgrade



### CERTIFICATES¶
# You'll need to install the ZScaler certificate authority into WSL to use the ZScaler proxy. This can be done as follows:
# Connect to the VPN
# Open up a dunnhumby internal URL in your browser (steps for downloading are for chromium browsers) - this one should work # https://test12-ap-wharf.anal-dev.dunnhumby.cloud/
# Download the ZScaler certificate:
# Click the padlock in the URL bar
# Click 'Connection is secure'
# Click 'Certificate is valid'
# Click 'Certification Path'
# Click 'Zscaler Root CA'
# Click 'View Certificate'
# Click 'Details'
# Click 'Copy to File...'
# Select 'Base-64 encoded X.509 (.CER) and click next
# Choose a path to save to, naming the file zscaler-root-ca.cer

# Alternatively, run the following code in a PowerShell window on your PC

$OutputPath ="C:\temp\ZScalerCerts"
Remove-Item -Path $OutputPath -Recurse -Force | Out-Null
New-Item $OutputPath -ItemType Directory 
Get-ChildItem -Path cert: -Recurse | ? { $_.Subject -like "*zscaler root ca*" -and ($_.EnhancedKeyUsageList -notcontains "*Code Signing&") } | % {
    $Issuer =$_.IssuerName.Name
    $Thumbprint =$_.Thumbprint
    Write-Output "Exporting zscaler cert $Issuer with thumbprint $Thumbprint"    
    $content =@(
    '-----BEGIN CERTIFICATE-----'
    [System.Convert]::ToBase64String($_.RawData, 'InsertLineBreaks')
    '-----END CERTIFICATE-----'
    )
    Set-Content -Path "$($OutputPath)\$($Thumbprint).crt" -Value $content -Encoding ascii
 }
Remove-Item "$($OutputPath)\zscaler-root-ca.crt"
Get-ChildItem -Path $OutputPath -Recurse |  % {
    Get-Content -Path $_.FullName | Out-File -Append "$($OutputPath)\zscaler-root-ca.crt" -Encoding ascii
} 

$OutputPath ="C:\temp\dunnhumbyCerts"
Remove-Item -Path $OutputPath -Recurse -Force | Out-Null
New-Item $OutputPath -ItemType Directory 
Get-ChildItem -Path cert: -Recurse | ? { $_.Subject -like "*dunnhumby root ca*" -and ($_.EnhancedKeyUsageList -notcontains "*Code Signing&") } | % {
    $Issuer =$_.IssuerName.Name
    $Thumbprint =$_.Thumbprint
    Write-Output "Exporting dunnhumby cert $Issuer with thumbprint $Thumbprint"    
    $content =@(
    '-----BEGIN CERTIFICATE-----'
    [System.Convert]::ToBase64String($_.RawData, 'InsertLineBreaks')
    '-----END CERTIFICATE-----'
    )
    Set-Content -Path "$($OutputPath)\$($Thumbprint).crt" -Value $content -Encoding ascii
 }
Remove-Item "$($OutputPath)\dunnhumby-root-ca.crt"
Get-ChildItem -Path $OutputPath -Recurse |  % {
    Get-Content -Path $_.FullName | Out-File -Append "$($OutputPath)\dunnhumby-root-ca.crt" -Encoding ascii
} 


# In WSL2, run the following to copy the certificate to WSL2
sudo bash
cp -p /mnt/c/temp/ZScalerCerts/zscaler-root-ca.crt /usr/local/share/ca-certificates/zscaler-root-ca.crt
cp -p /mnt/c/temp/dunnhumbyCerts/dunnhumby-root-ca.crt /usr/local/share/ca-certificates/dunnhumby-root-ca.crt

# Now run this command to update certificates:
update-ca-certificates
# Run the following to confirm installation succeeded:
awk -v cmd='openssl x509 -noout -subject' '/BEGIN/{close(cmd)};{print | cmd}' < /etc/ssl/certs/ca-certificates.crt | grep -i zscaler
# If installation was successful, you should see output like subject=C = US, ST = California, L = San Jose, O = Zscaler Inc., OU = Zscaler Inc., CN = Zscaler Root CA, emailAddress = support@zscaler.com
awk -v cmd='openssl x509 -noout -subject' '/BEGIN/{close(cmd)};{print | cmd}' < /etc/ssl/certs/ca-certificates.crt | grep -i dunnhumby
# If installation was successful, you should see output like subject=C = GB, O = Dunnhumby, CN = Dunnhumby Root CA









### Docker
# Install Docker following instructions at https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
# (In the instructions the command "service docker start" might be missing to start the docker service)

# In case the docker service doesn't start automatically, you might need to add the following lines to the /etc/wsl.conf file. 
# This might no longer be required after the latest WSL2 update from Microsoft Store, which installs systemd. If that's the case, follow the instructions below
	[boot]
	systemd = true

# If systemd is available instead, docker is installed as a service that you can control using the standard systemctl commands
# To make docker start automatically when WSL starts, you can run the command
systemctl enable docker


# Add your user to the "docker" group, so you can run docker commands
sudo adduser marcol docker


# At this point you probably need to reboot your laptop so the systemd can be enabled at the startup


# Set the proxy for docker

sudo bash
mkdir /etc/systemd/system/docker.service.d
# Now create a file called /etc/systemd/system/docker.service.d/http-proxy.conf that adds the HTTP_PROXY and HTTPS_PROXY environment variables:
cat <<EOF > /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://165.225.80.41:80/"
Environment="HTTPS_PROXY=http://165.225.80.41:80/"
Environment="NO_PROXY=.dunnhumby.co.uk,.dunnhumby.com,*.dunnhumby.co.uk,*.dunnhumby.com,.kssretail.local,localhost,127.0.0.1,tunnel.cloudproxy.app,10.0.2.15,tunnel.cloudproxy.iap,.googleapis.com,*.googleapis.com,googleapis.com,.cloud.google.com,*.cloud.google.com,gcr.io,eu.gcr.io,us.gcr.io,asio.gcr.io,.gcr.io,192.168.49.2"
EOF

#Flush changes:
systemctl daemon-reload

#Verify that the configuration has been loaded:
systemctl show --property Environment docker

#Restart Docker
systemctl restart docker



##### VSCode

# Get started using Visual Studio Code with Windows Subsystem for Linux
# https://learn.microsoft.com/en-us/windows/wsl/tutorials/wsl-vscode

# Raise a software request for VSCode
# Once installed run this to add VSCode to the path
cat <<EOF > /etc/profile.d/add-vscode-path.sh
export PATH=\$PATH:"/mnt/c/Program Files/Microsoft VS Code/bin"
EOF

#Then add few extensions from VSCode extensions section
#   - Python
#   - Python Docs
#   - Docker
#   - Gitlens
#   - autoDocstring
#   - Remote Development



# In general from WSL you type the command "code ." and that starts a VSCode windows on your laptop, with the current working directory open in VSCode





# START SECTION: this is for the python instance that comes with Ubuntu 
#   (you might not need this if all the work is done inside containers)

### pip installation  
# In WSL 
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py

###  CONDA 
# Go to https://docs.conda.io/en/latest/miniconda.html and get the link to the Miniconda installer 
# you are interested in (e.g. https://repo.anaconda.com/miniconda/Miniconda3-py39_4.12.0-Linux-x86_64.sh)

# In WSL
wget https://repo.anaconda.com/miniconda/Miniconda3-py310_23.5.2-0-Linux-x86_64.sh
sudo bash Miniconda3-py310_23.5.2-0-Linux-x86_64.sh -b -p /opt/conda
rm Miniconda3-py310_23.5.2-0-Linux-x86_64.sh

# initialise conda env, then restart the terminal for it to take effect
/opt/conda/bin/conda init



sudo bash
/opt/conda/bin/conda config --system --set auto_update_conda false
/opt/conda/bin/conda config --system --set show_channel_urls true
/opt/conda/bin/conda config --system --set channel_alias https://cs-anonymous:Welcome123@artifactory.dunnhumby.com/artifactory/api/conda
/opt/conda/bin/conda config --system --remove channels defaults
/opt/conda/bin/conda config --system --add channels conda-main-remote
/opt/conda/bin/conda config --system --append channels conda-dsp-local
/opt/conda/bin/conda config --system --append channels conda-forge-remote
/opt/conda/bin/conda config --system --append channels conda-r-remote
/opt/conda/bin/conda config --system --set env_prompt '({name}) '

conda install -c conda-forge-remote --quiet --yes --all mamba conda-build cachecontrol lockfile ripgrep conda-pack
exit

# Initialise conda for your user
mamba init

# END SECTION: this is for the python instance that comes with Ubuntu




####### Minikube ##########

# Install Minikube in the WSL environment
sudo bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
dpkg -i minikube_latest_amd64.deb
exit

# Add an alias for the kubectl command
cat <<EOF >> ~/.bashrc

# Alias for kubectl command
alias kubectl="minikube kubectl --"
EOF
. ~/.bashrc


# Minikube requires CA certs in its own location (under ~/.minikube/certs)
cp -p /usr/local/share/ca-certificates/zscaler-root-ca.crt /usr/local/share/ca-certificates/dunnhumby-root-ca.crt ~/.minikube/certs


# Start minikube (requires specific IP ranges used by minikube to not go through the proxy
export NO_PROXY=$NO_PROXY,10.96.0.0/12,192.168.59.0/24,192.168.49.0/24,192.168.39.0/24
export no_proxy=$NO_PROXY
minikube start
minikube addons enable metrics-server

# We want docker to use the registry inside minikube
eval $( minikube -p minikube docker-env )

# Get status of pods
kubectl get po -A

# Start the dasboard UI
# Manually downloading this image because it takes longer than the timeouts allow 
#      (let the command run, even if at some point it seems it's stuck for few minutes)
docker pull docker.io/kubernetesui/dashboard:v2.7.0
minikube dashboard --url
# Then open in the browser the URL printed on the console



# To delete the dashboard namespace
kubectl delete namespace kubernetes-dashboard

# To restart from scratch the minikube server
minikube delete
minikube start
eval $( minikube -p minikube docker-env )


# To point back docker to the local repo outside minikube
eval $( minikube -p minikube docker-env -u )


# Create a deployment
kubectl create deployment hello-node \
    --image=registry.k8s.io/e2e-test-images/agnhost:2.39 \
	     -- /agnhost netexec --http-port=8080

# Exposes the deployment
kubectl expose deployment hello-node --type=LoadBalancer --port=8080

# Creates a tunnel for the exposed service
minikube service hello-node

kubectl get deployments,pods,services

# Removes the service
kubectl delete service hello-node
# Removes the deployment
kubectl delete deployment hello-node





###### HELM ######

# Install Helm in WSL2 from apt (see https://helm.sh/docs/intro/install/#from-apt-debianubuntu)
sudo bash
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm

# Get started with creating Helm Charts: https://helm.sh/docs/chart_template_guide/

helm create part6-chart
cd part6-chart




###### TERRAFORM ##########
# Installation instructions at https://developer.hashicorp.com/terraform/tutorials/gcp-get-started/install-cli
sudo bash 
apt-get update 
apt-get install -y gnupg software-properties-common
apt-get install terraform vault consul nomad packer
exit

# enable auto-completion for Terraform
terraform -install-autocomplete




###### Google Cloud SDK ########

sudo apt-get install apt-transport-https ca-certificates gnupg
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
sudo apt-get update && sudo apt-get install google-cloud-cli

gcloud init
gcloud auth login
gcloud auth application-default login

sudo apt-get update
sudo apt-get install -y kubectl
sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin

# Add variables ARTIFACTORY_USER and ARTIFACTORY_KEY to your .bashrc file




########  GPU support  ##########
# Useful links: 
# - https://ubuntu.com/tutorials/enabling-gpu-acceleration-on-ubuntu-on-wsl2-with-the-nvidia-cuda-platform#1-overview
# - https://learn.microsoft.com/en-us/windows/wsl/tutorials/gpu-compute
# - https://www.nvidia.com/Download/index.aspx
# - https://saturncloud.io/blog/how-to-use-gpu-from-a-docker-container-a-guide-for-data-scientists-and-software-engineers/
# - https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/user-guide.html
# - https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html


# Download and install the latest driver for your NVIDIA GPU from https://www.nvidia.com/Download/index.aspx
#   For NVIDIA T600 the driver can be downloaded from https://www.nvidia.com/Download/driverResults.aspx/210654/en-us/, after accepting the final download URL is the following 
#   https://us.download.nvidia.com/Windows/Quadro_Certified/537.13/537.13-quadro-rtx-desktop-notebook-win10-win11-64bit-international-dch-whql.exe
#   (note: I had to run the installation twice for the GPU to be visible again)

#  install the NVIDIA Container Toolkit
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-docker-keyring.gpg
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-docker-keyring.gpg] https://#g' | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update
sudo apt-get install -y nvidia-docker2

sudo nvidia-ctk runtime configure --runtime=docker
sudo systemctl restart docker

# test 
docker run --rm --runtime=nvidia --gpus all nvidia/cuda:12.2.0-base-ubuntu20.04 nvidia-smi



###### TROUBLESHOOTING

### Issues starting WSL
# in case you get an error "Logon failure: the user has not been granted the requested logon type at this computer" 
# you might need to restart the vmcompute service. Run PowerShell as admin and then run this:
restart-service vmcompute 

# Be careful however... several times, running the command above after some time I got the BSOD!
# I've not needed to run the command above since I got the new laptop, fingers crossed!




