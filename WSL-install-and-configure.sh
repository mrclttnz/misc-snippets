
# Instructions at https://learn.microsoft.com/en-us/windows/wsl/install and https://learn.microsoft.com/en-us/windows/wsl/setup/environment

# Start Powershell as Admin and run the following

# Check Powershell version
Get-Host | Select-Object Version

# Upgrade Powershell (https://learn.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.4&WT.mc_id=THOMASMAURER-blog-thmaure#install-powershell-using-winget-recommended)
winget search Microsoft.PowerShell
winget install --id Microsoft.Powershell --source winget

# Install WSL version 2  (https://learn.microsoft.com/en-us/windows/wsl/install-manual)

# Enable the Windows Subsystem for Linux
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

# Enable Virtual Machine feature
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

# Reboot the computer !


# Download latest WSL2 package (link at https://learn.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)
# likely the link is https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi, however check it at the page above
# execute the downloaded MSI file

# Set WSL 2 as your default version
wsl --set-default-version 2

# Update WSL 
wsl --update

# Reboot the computer !

# Install Ubuntu from Microsoft Store
# Open the Microsoft Store and select your favorite Linux distribution.
# Run Ubuntu from Windows Menu, then enter admin user info (userid/password)

# Upgrade system
sudo apt update && sudo apt upgrade

# Install WSL utilities
sudo apt install wslu

sudo apt-get install wget ca-certificates

# Install and configure git
sudo apt-get install git


# Install Windows Terminal from MS Store https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=it-it&gl=it&rtc=1


# Install Docker following instructions at https://docs.docker.com/engine/install/ubuntu/
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done

sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Verify that the Docker Engine installation is successful by running the hello-world image.
sudo docker run hello-world

# Add your user to the docker group, so you can run docker commands without using sudo
sudo adduser marco docker


# Install Visual Studio Code (instructions at https://learn.microsoft.com/en-us/windows/wsl/tutorials/wsl-vscode)
# Be sure to download the system installer, not the user's one
# When it launches, accept the installation of the WSL extension

# Install the Remote Development extension pack (https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack). This extension pack includes the WSL extension, in addition to the Remote - SSH, and Dev Containers extensions, enabling you to open any folder in a container, on a remote machine, or in WSL.

# Add VSCode path to the system path
sudo bash
cat <<EOF > /etc/profile.d/add-vscode-path.sh
export PATH=\$PATH:"/mnt/c/Program Files/Microsoft VS Code/bin"
EOF
exit

# Install the following VSCode extensions from the the extensions section
# python
# python docs
# Docker
# Gitlens
# autoDocstring
# Remote Development


# Install Conda in WSL (https://docs.conda.io/en/latest/miniconda.html)
# Download installer from 
https://repo.anaconda.com/miniconda/Miniconda3-py310_23.3.1-0-Linux-x86_64.sh
wget https://repo.anaconda.com/miniconda/Miniconda3-py310_23.3.1-0-Linux-x86_64.sh
sudo bash Miniconda3-py310_23.3.1-0-Linux-x86_64.sh -b -p /opt/conda
rm Miniconda3-py310_23.3.1-0-Linux-x86_64.sh

# Install and configure mamba on top of conda
sudo /opt/conda/bin/conda install -c conda-forge --yes --all mamba conda-build cachecontrol lockfile ripgrep conda-pack

/opt/conda/bin/mamba init
